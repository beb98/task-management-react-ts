import React, {useContext, createContext, useState} from 'react';
import { getLocalState, setLocalState } from '../helper/LocalStorage/localStorage';
import { LocalStorageCommands } from '../helper/LocalStorage/localStorageCommands';

const TasksListContext = createContext<any[]>([]);
// use tasks list store function to return the state values [tasksList , setTasksList]
export const useTasksListStore = () => useContext(TasksListContext);

const TasksListStore : React.FC = (props) => {
    
    // Get the initial state if the user has an old tasks
    const [tasksList , setTasksList] = useState<boolean>(
                                                    getLocalState(LocalStorageCommands.tasks) 
                                                    ?? []
                                                );

    // on change > save the new tasks list in the storage
    React.useEffect(() => {
        setLocalState(LocalStorageCommands.tasks, tasksList);
    }, [tasksList])
    
    return (
        <TasksListContext.Provider value={[tasksList , setTasksList]}>
                {props.children}
        </TasksListContext.Provider>
    );
}

export default TasksListStore;