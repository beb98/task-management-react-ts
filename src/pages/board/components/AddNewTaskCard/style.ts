import { makeStyles } from "@material-ui/core";
import { colors } from "../../../../theme/colors";

export const useTasksCardStyles = makeStyles((theme) => ({
    Card : {        
        background: colors.white,
        borderRadius: "5px",
        animation: "cardAppear 0.8s ease-in-out",
        boxShadow: "0px 3px 6px rgba(0,0,0,0.06)",
        padding : "1rem",
        marginTop: "1rem"
    },
    CardInput: {
        marginTop : "0.8rem",
        border : "0px",
        borderColor: colors.transparent
    },
    cardActions: {
        marginTop : "1rem",
        animation: "cardAppear 0.8s ease-in-out"
    }
}));