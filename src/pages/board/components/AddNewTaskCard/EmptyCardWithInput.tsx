import React from 'react';
import { Button, Card, IconButton, TextField, Typography } from '@material-ui/core';
import {FiX} from 'react-icons/fi';
import { useTasksCardStyles } from './style';

interface IProps{
    value: string,
    setTaskText: Function,
    onAdd: Function,
    newTaskValueError: boolean,
    setAddMode: Function
}

// React.FC, EmptyCardWithInput 
const NewCardWithInput : React.FC<IProps> = (Props) => {
    const classes = useTasksCardStyles();

    const handleKeyDown = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            Props.onAdd(Props.value);
        }
    }

    return(
        <Card elevation={5} className={classes.Card} data-test="material-card-test-new">
            <Typography>New Task</Typography>
            <TextField  
                placeholder="Task Text"
                variant="standard"
                color="primary"
                size="small"
                autoFocus
                className={classes.CardInput}
                value={Props.value}
                onChange={(e) => Props.setTaskText(e.target.value)}
                onKeyDown={handleKeyDown}
                error={Props.newTaskValueError}
                helperText={Props.newTaskValueError && "Task text can not be empty"}
                data-test="add-new-task-input"
            />
            <div className={classes.cardActions}>
                <Button variant="contained" color="secondary" 
                        onClick={() => Props.onAdd(Props.value)} 
                        data-test="add-new-task-button">
                    Add
                </Button>
                <IconButton onClick={() => Props.setAddMode(false)} >
                    <FiX/>
                </IconButton>
            </div>
        </Card>
    )
}

export {NewCardWithInput};