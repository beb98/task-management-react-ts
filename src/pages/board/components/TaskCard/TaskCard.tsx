import React from 'react';
import { Button, Card, IconButton, TextField } from '@material-ui/core';
import { Task } from '../../../../models/Task';
import {FiX, FiEdit, FiCalendar} from 'react-icons/fi';
import { useTasksCardStyles } from './style';
import { Draggable } from "react-beautiful-dnd";
import { TasksTypes } from '../../../../models/enums/TasksTypes';
import { TaskHistoryDialog } from '../../../../components/TaskHistoryDialog/TaskHistoryDialog';

interface TaskActions {
    onEdit : Function,
    onDelete : Function,
    index: number
}
// Inherit the task interface and the task actions interface as Props
interface IProps extends Task, TaskActions {}

// React.FC
const TaskCard : React.FC<IProps> = (Props) => {
    const classes = useTasksCardStyles();
    // Local state
    const [isHiddenActions, setIsHiddenActions] = React.useState<boolean>(true);
    //      Edit mode state
    const [editMode, setEditMode] = React.useState<boolean>(false);
    const [editModeValue, setEditModeValue] = React.useState<string>(Props.text);
    const [editModeError, setEditModeError] = React.useState<boolean>(false);
    // History Dialog controls state
    const [isHistoryDialogOpened, setIsHistoryDialogOpened] = React.useState<boolean>(false);
  
    // On click Enter, to edit the card
    const handleKeyDown = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            editCardText();
        }
    }

    const editCardText = () => {
        // Check if the value is not empty
        if(!editModeValue || editModeValue.replace(/\s/g, '').length === 0) {
            setEditModeError(true);
            return;
        }

        // we do it here because we're working with localstorage and localstate, no async methods or async work needed
        Props.onEdit(Props.id, editModeValue);
        setEditMode(false);
    }

    // Close history dialog
    const handleCloseDialog = () => {
        setIsHistoryDialogOpened(false);
    }

    return(
        <Draggable
            draggableId={Props.id}
            index={Props.index}
            isDragDisabled={Props.type === TasksTypes.done}
        >
        {(provided : any) => (
        <div 
            onMouseEnter={() => setIsHiddenActions(false)}
            onMouseLeave={() => setIsHiddenActions(true)}  
            {...provided.dragHandleProps}
            {...provided.draggableProps}
            ref={provided.innerRef}
            data-test="material-card-test"
        >
            <Card elevation={5} className={classes.Card} data-test="task-card"                                 
                                // Give a drag option for only toDo and inProgres
                                // Give move cursor only for toDo and inProgress
                                style={Props.type === TasksTypes.done ? {cursor:"pointer"} : {}}>
                {
                    !isHiddenActions ? 
                        <div className={classes.cardActionsTop} data-test="card-actions">
                            <IconButton size="small" onClick={() => setIsHistoryDialogOpened(true)}
                                        test-data="history-icon-button">
                                <FiCalendar size={15} />
                            </IconButton>
                            {
                                Props.type !== TasksTypes.done &&
                                <IconButton size="small" onClick={() => setEditMode(!editMode)} 
                                            className={classes.cardActionsTopAction}
                                            data-test="edit-icon-button">
                                    <FiEdit size={15}/>
                                </IconButton>   
                            }
                            <IconButton size="small" className={classes.cardActionsTopActionDelete}
                                        onClick={() => Props.onDelete(Props.id)} >
                                <FiX size={15}/>
                            </IconButton>
                        </div>
                        :
                        <div className={classes.cardActionsTopPlaceholder}></div>
                }

                {/* view/edit current task */}
                {
                    editMode ? 
                    <div data-test="edit-task-container-test">
                        <TextField  
                            placeholder="Task Text"
                            variant="standard"
                            color="primary"
                            size="small"
                            className={classes.CardInput}
                            value={editModeValue}
                            onChange={(e) => setEditModeValue(e.target.value)}
                            onKeyDown={handleKeyDown}
                            error={editModeError}
                            helperText={editModeError && "Task text can not be empty"}
                            data-test="edit-mode-input"
                            name="text"
                        />
                        <div className={classes.editModeCardActions}>
                            <Button variant="contained" color="secondary" 
                                    onClick={editCardText} 
                                    data-test="submit-edit-value">
                                Edit
                            </Button>
                            <IconButton onClick={() => setEditMode(false)} >
                                <FiX/>
                            </IconButton>
                        </div>
                    </div>
                    :
                    Props.text
                }                
            </Card>          

            {/* History Dialog */}
            {
                isHistoryDialogOpened && 
                <TaskHistoryDialog open={isHistoryDialogOpened} 
                               onClose={handleCloseDialog} 
                               task={{
                                    text : Props.text,
                                    history: Props.history,
                                    id : Props.id,
                                    type : Props.type,
                                    editesHistory: Props.editesHistory
                                }} 
                                test-data="dialog-test"
                />
            }            
        </div>
        )}
        </Draggable>
    )
}

export {TaskCard};