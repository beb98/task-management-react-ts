import { makeStyles } from "@material-ui/core";
import { colors } from "../../../../theme/colors";

export const useTasksCardStyles = makeStyles((theme) => ({
    Card : {        
        background: colors.white,
        borderRadius: "5px",
        boxShadow: "0px 3px 6px rgba(0,0,0,0.06)",
        padding : "0px 1rem 1rem 1rem",
        transition: "all 0.5s ease-in-out",
        marginTop: "1rem",
        cursor: "move"
    },
    CardInput: {
        marginTop : "0.8rem",
        border : "0px",
        borderColor: colors.transparent
    },
    cardActionsTop: {
        width: "100%",
        display: "flex",
        justifyContent: "flex-end",
        marginTop: "0.8rem",
        marginBottom : "0.5rem",
        animation : "cardActionsAppear 0.5s ease-in-out"
    },
    cardActionsTopPlaceholder:{
        height : "2.74rem"
    },
    cardActionsTopAction: {
        marginLeft: "0.8rem"
    },
    cardActionsTopActionDelete:{
        color: colors.error,
        marginLeft: "0.5rem"
    },
    editModeCardActions: {
        marginTop : "1rem",
        animation: "cardAppear 0.2s ease-in-out"
    }
}));