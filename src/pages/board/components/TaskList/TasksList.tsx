import React from 'react';
import { Button, Card, Typography } from '@material-ui/core';
import { useTasksListStore } from '../../../../contexts/tasks.store';
import { TasksTypes } from '../../../../models/enums/TasksTypes';
import { Task } from '../../../../models/Task';
import { TasksListProps } from '../../../../models/TasksList';
import { TaskCard } from '../../components/TaskCard/TaskCard';
import { useTasksListStyles } from './style';
import {FiPlus} from 'react-icons/fi';
import { NewCardWithInput } from '../AddNewTaskCard/EmptyCardWithInput';
import { GenerateGuid } from '../../../../helper/GUID';
// React DND
import { Droppable } from "react-beautiful-dnd";

const TasksList : React.FC<TasksListProps> = (Props) => {

    const classes = useTasksListStyles();
    // local state
    //      Add mode state
    const [addMode, setAddMode] = React.useState<boolean>(false);
    const [newTaskValue, setNewTaskValue] = React.useState<string>("");
    const [newTaskValueError, setNewTaskValueError] = React.useState<boolean>(false);
    // global tasks list
    const [tasksList, setTasksList]  = useTasksListStore();    

    //#region set error to false if the user starting to type after firing it
    React.useEffect(() => {
        if (newTaskValue) {
            setNewTaskValueError(false);
        }
    }, [newTaskValue])
    //#endregion

    //#region Add new task
    const addNewTask = (taskText: string) : void => {
        // check if the value is empty, set error and stop the function
        if (!taskText || taskText.replace(/\s/g, '').length === 0) {
            setNewTaskValueError(true);
            return;
        }
        // Create new task object based on task Model
        const newTaskToAdd : Task = {
            id: GenerateGuid(),
            history: [],
            text : taskText,
            type : TasksTypes.toDo,
            editesHistory: [{
                new: taskText,
                old : null
            }]
        }
        // Update the whole board state (Global state)
        setTasksList([
            ...tasksList,
            newTaskToAdd        
        ])
        // remove current value to add new task 
        setNewTaskValue("");
    }

    //#region edit task
    const EditTaskText = (taskId : string, newValue: string) : void => {
        try {
            const index = tasksList.findIndex((t : Task) => t.id === taskId);
            let copyOfTasksList : Task[] = [...tasksList];
                        
            copyOfTasksList[index]['editesHistory'].push({
                old: copyOfTasksList[index]['text'],
                new : newValue
            });

            copyOfTasksList[index]['text'] = newValue;
            setTasksList(copyOfTasksList);
        }
        catch(e) {
            // Ex handling. route to Internal error
            // will replace it with console.error for now 
            console.error('Internal error: TasksList.tsx Line:62');
        }
    }
    //#endregion

    //#region edit task
    const DeleteTask = (taskId : string) : void => {
        try {
            const index = tasksList.findIndex((t : Task) => t.id === taskId);
            let copyOfTasksList : Task[] = [...tasksList];
                        
            copyOfTasksList.splice(index, 1);
            setTasksList(copyOfTasksList);
        }
        catch(e) {
            // Ex handling. route to Internal error
            // will replace it with console.error for now 
            console.error('Internal error: TasksList.tsx Line:79');
        }
    }
    //#endregion
    
    return(
        <Droppable droppableId={Props.id}>
        {(provided: any) => (
            <Card className={classes.listContainer}
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    data-test="material-card-test"
                    data-testid={Props.id}>
            {/* Title */}
            <Typography variant="h6" className={classes.heading}>{Props.title}</Typography>            
            
            {/* Tasks with type as the props type */}
            {
                tasksList?.map((task : Task, index: number) => (
                    // Render only the task with type as prop type
                    task.type === Props.type &&
                    <TaskCard key={task.id} 
                              {...task} 
                              index={index}
                              onEdit={EditTaskText} 
                              onDelete={DeleteTask} 
                    />                    
                ))
            }

            {/* Add new Task with same prop type, 
            *** Available only for toDo type*/}
            {
                Props.type === TasksTypes.toDo &&
                <div>
                    {
                        addMode &&
                        <NewCardWithInput value={newTaskValue} setTaskText={setNewTaskValue} onAdd= {addNewTask} newTaskValueError={newTaskValueError} setAddMode={setAddMode}
                        data-test="new-task-card-with-input"/>
                    }
                    <Button variant="text" className={classes.addNewTaskButton} 
                            onClick={() => setAddMode(true)} id="addNewTaskCard" 
                            data-test="open-add-mode">
                        <FiPlus size={15} className={classes.addNewTaskButtonIcon} />
                        <span>Add another task card</span>
                    </Button>
                </div>
            }
        </Card>
        )}
        </Droppable>
    )
}


export {TasksList};