import { makeStyles } from "@material-ui/core";
import { colors } from "../../../../theme/colors";

export const useTasksListStyles = makeStyles((theme) => ({
    listContainer : {
        marginTop :"2rem",
        height: "auto",
        padding: "1.5rem 0.8rem",
        width: "20vw",
        marginLeft: "2rem",
        background: colors.lightGrey,
        borderRadius: "5px"        
    },
    heading : {
        marginBottom : "1.2rem"
    },
    addNewTaskButton : {        
        width : "100%",
        marginTop :"1rem",
        transform : "translateY(0.5rem)"        
    },
    addNewTaskButtonIcon: {
        marginRight : "0.5rem",
        transform : "translateY(-0.16rem)"        
    }
}));