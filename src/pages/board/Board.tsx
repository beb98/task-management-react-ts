import React from 'react';
import { TasksList } from './components/TaskList/TasksList';
import { useBoardStyles } from './style';
import { useTasksListStore } from '../../contexts/tasks.store';
// Models and enums
import { TasksTypes } from '../../models/enums/TasksTypes';
// React Dnd
import { DragDropContext } from "react-beautiful-dnd";
import { Task } from '../../models/Task';
import { formatDate } from '../../helper/Date';
import { ErrorAlert, InfoAlert } from '../../helper/Toast/toast';

interface IProps{
    testInitialData?: Task[];
}
const BoardView : React.FC<IProps> = (Props) => {   
    const classes = useBoardStyles();
    const [tasksList, setTasksList] = useTasksListStore();
    const boardLists : any[] = [
        {title : "Todo", type: TasksTypes.toDo, id : "list-1"},
        {title : "In progress", type: TasksTypes.inProgress, id : "list-2"},
        {title : "Done", type: TasksTypes.done, id: "list-3"}
    ];


    // DnD Test cards
    React.useEffect(() => {
        if (Props.testInitialData) {
            setTasksList([...Props.testInitialData]);
        }
    }, [])
    
    const getTaskListTitle = (taskListType: TasksTypes) => {
        const task = boardLists.find((t) => t.type === taskListType);
        return task.title;
    }

    // On drop function
    const onDragEnd = (dragEndResult: any) => {
        const { draggableId, source, destination } = dragEndResult;
        // Catch errors and stop the drop
        if (!draggableId) return;
        if (!source) return;
        if (!destination) return;

        switch(source.droppableId) {
            case(TasksTypes.toDo):
                dropTodo(draggableId, destination.droppableId, source.index);
            break;
            case(TasksTypes.inProgress):
                dropInProgress(draggableId, destination.droppableId, source.index);
            break;
        }
    }

    //#region Drop Card to list 
        //#region Drop Todo to Inporgress only
    const dropTodo = (itemId: string, listType: string, index: number) => {
        if (listType === TasksTypes.inProgress) {
            try {
                const index = tasksList.findIndex((t : Task) => t.id === itemId);
                let copyOfTasksList : Task[] = [...tasksList];                
                                                
                copyOfTasksList[index]['type'] = TasksTypes.inProgress;
                copyOfTasksList[index]['history'].push({
                    from : "Todo",
                    to : getTaskListTitle(TasksTypes.inProgress),
                    date : formatDate(Date.now())
                });
                
                setTasksList(copyOfTasksList);
                // give the user some info
                InfoAlert(`Dropped in ${getTaskListTitle(TasksTypes.inProgress)} List`);
            }
            catch(e) {                    
                // Ex handling. route to Internal error
                // will replace it with console.error for now 
                console.error('Internal error: TasksList.tsx Line:62');
            }
        }       
        else if (listType === TasksTypes.done)
        {
            ErrorAlert('Can not be done, Move it to in progress list first');
        }     
    }
        //#endregion
    
        //#region Drop inProgress to Todo and Done
    const dropInProgress = (itemId: string, listType: string, index: number) => {
        if (listType === TasksTypes.toDo || listType === TasksTypes.done) {
            try {
                const index = tasksList.findIndex((t : Task) => t.id === itemId);
                let copyOfTasksList : Task[] = [...tasksList];                
                            
                copyOfTasksList[index]['type'] = listType;
                copyOfTasksList[index]['history'].push({
                    from : "In progress",
                    to : getTaskListTitle(listType),
                    date : formatDate(Date.now())
                });

                setTasksList(copyOfTasksList);
                InfoAlert(`Dropped in ${getTaskListTitle(listType)} List`);
            }
            catch(e) {                    
                // Ex handling. route to Internal error
                // will replace it with console.error for now 
                console.error('Internal error: TasksList.tsx Line:62');
            }
        }   
    }        
        //#endregion
    //#endregion

    return (
        <div className={classes.boardContainer}>           
                    <div className={classes.listsContainer}>
                        {
                            <DragDropContext onDragEnd={onDragEnd}>
                                {boardLists?.map((list, index) => (
                                    <TasksList key={index} title={list.title} type={list.type} id={list.type} />
                                ))}
                            </DragDropContext>
                        }                
                    </div>
        </div>
    )
}

export {BoardView};