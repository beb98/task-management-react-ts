import { makeStyles } from "@material-ui/core";
import { colors } from "../../theme/colors";

export const useBoardStyles = makeStyles((theme) => ({
    boardContainer : {
        background : colors.boardBackground,
        height : "auto",
        minHeight: "100vh",
        width : "100vw",
        padding : "2rem"
    },
    listsContainer : {
        width : "100vw",
        maxWidth: "auto",
        overflowX: "scroll",
        display: 'flex',
        justifyContent : "flex-start"
    }
}));