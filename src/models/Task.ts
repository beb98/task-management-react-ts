import { TasksTypes } from "./enums/TasksTypes";

export interface TaskHistory {
    from : string, 
    to : string, 
    date? : string
}

export interface EditesHistory {
    old : string | null, 
    new: string
}

export interface Task {
    text: string,
    type: TasksTypes,
    history : TaskHistory[],
    editesHistory: EditesHistory[],
    id: string
}