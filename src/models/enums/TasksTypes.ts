export enum TasksTypes { 
    toDo = "toDo",
    inProgress = "inProgress",
    done = "done"
}
