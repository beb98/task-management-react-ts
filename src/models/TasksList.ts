import { TasksTypes } from "./enums/TasksTypes";

export interface TasksListProps {
    title: string,
    type: TasksTypes,
    id: string
}