import React from "react";
import { mount } from "enzyme";
import {NewCardWithInput} from '../pages/board/components/AddNewTaskCard/EmptyCardWithInput';

// Start tests for <TasksList /> component
describe("<TasksList />", () => {
    // Create and return wrapper with user tasks list store (context api store)
    const designedAndGetWrapper = () =>
        mount(
        <NewCardWithInput newTaskValueError={false} 
                          onAdd={() => {}} 
                          value={'Input value'} 
                          setAddMode={() => {}}
                          setTaskText={() => {}} />
    );

    const wrapper = designedAndGetWrapper();

    wrapper.update();
    // should match the snapshot test
    it("should match the snapshot", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    
    //  is to make sure that the component rendered the card UI successfully
    it("should render a material ui paper as a card", () => {
        expect(wrapper.find("[data-test='material-card-test-new']").exists())
        .toBe(true);
    });

    //  should render an Add button for the new cards
    it("should render an Add button for the new cards", () => {
        expect(wrapper.find("[data-test='add-new-task-button']").exists())
        .toBe(true);
    });

   //  should render an Add button for the new cards
   it("should render an Add button for the new cards", () => {
        expect(wrapper.find("[data-test='add-new-task-input']").exists())
        .toBe(true);
    });



});
