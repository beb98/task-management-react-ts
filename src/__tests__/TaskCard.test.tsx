import React from "react";
import { mount } from "enzyme";
import TasksListStore from "../contexts/tasks.store";
import { TaskCard } from "../pages/board/components/TaskCard/TaskCard";
import { TasksTypes } from "../models/enums/TasksTypes";
import sinon from 'sinon';

// Create drag and drop mock using jest 
jest.mock('react-beautiful-dnd', () => ({
  Droppable: ({ children } : any) => children({
    draggableProps: {

    },
    innerRef: jest.fn()    
  }, {}),
  Draggable: ({ children } : any) => children({
    draggableProps: {

    },
    innerRef: jest.fn()    
  }, {}),
  // get drag drop context children
  DragDropContext: ({ children } : any) => children,
}));

// Spy for simulation 
const clickInputSpy = sinon.spy(HTMLInputElement.prototype, 'click');
// Start tests for <TaskCard /> component
describe("<TaskCard />", () => {
  // Create and return wrapper with user tasks list store (context api store)
  const designedAndGetWrapper = (props: any) => mount(
        <TasksListStore>
          <TaskCard {...props} />
        </TasksListStore>
      );

  const wrapper = designedAndGetWrapper({
    index: 0,
    onClick: (id: string) => console.log(id),
    classes: {},
    id: "card-test-1",
    text: "Task text",
    type: TasksTypes.toDo,
    history: [],
    editesHistory: [],
    onEdit: (id: string, newValue: string) => {      
      wrapper.setProps({ editModeValue: newValue });
    }    
  });

  wrapper.update();
  // is to make sure that the component is still the same
  it("should match the snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  //  is to make sure that the component rendered the card UI successfully
  it("should render a material ui paper as a card", () => {
    expect(wrapper.find("[data-test='material-card-test']").exists())
    .toBe(true);
  });

  //  is to make sure that we have edit button but not rendered
  it("should not render the edit button at the first time", () => {
    expect(wrapper.find("[data-test='edit-task-test']").exists())
    .toBe(false);
  });

  //  is to make sure that we have card actions icon buttons after fire mouse enter event
  it("should have card actions after mouse enter the card", () => {
    expect(wrapper.find("[data-test='material-card-test']")
    .simulate('mouseenter'));

    expect(wrapper.find("[data-test='card-actions']").exists())
    .toBe(true);
  });

  // after edit icon button appeares, we will click on on it and check if the edit mode is on 
  it("should have the edit mode container after click on edit icon button", () => {    
    expect(wrapper.find("[data-test='edit-icon-button']")
    .at(0)
    .simulate('click'));

    expect(wrapper.find("[data-test='edit-task-container-test']").exists())
    .toBe(true);
  });

  // should change the input value in the edit mode box
  it('Should find the edit mode input', () => {
    expect(wrapper.find("[data-test='edit-mode-input']").exists())
    .toBe(true);
  })

  // should change the input value in the edit mode box
  it("Value of edit input should be as task prop value", () => { 
    expect(wrapper.find("[data-test='edit-mode-input']")
    .at(0)
    .prop('value'))
    .toEqual("Task text");
  });
  
  // should change the input value in the edit mode box
  it("Value of edit input should be as task prop value", () => { 
    expect(wrapper.find("[data-test='edit-mode-input']")
    .at(0)
    .prop('value'))
    .toEqual("Task text");
  });
});
