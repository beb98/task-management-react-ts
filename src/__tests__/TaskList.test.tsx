import React from "react";
import { mount } from "enzyme";
import TasksListStore from "../contexts/tasks.store";
import { TasksList } from "../pages/board/components/TaskList/TasksList";
import { TasksTypes } from "../models/enums/TasksTypes";

// Create drag and drop mock using jest 
jest.mock('react-beautiful-dnd', () => ({
  Droppable: ({ children } : any) => children({
    draggableProps: {
      style: {},
    },
    innerRef: jest.fn()    
  }, {}),
  Draggable: ({ children } : any) => children({
    draggableProps: {
      style: {},
    },
    innerRef: jest.fn()    
  }, {}),
  // get drag drop context children
  DragDropContext: ({ children } : any) => children,
}));

// Start tests for <TasksList /> component
describe("<TasksList />", () => {
    // Create and return wrapper with user tasks list store (context api store)
    const designedAndGetWrapper = (props: any) =>
        mount(
        <TasksListStore>
            <TasksList {...props}  />
        </TasksListStore>
    );

    const wrapper = designedAndGetWrapper({
        index: 0,
        onClick: (id: string) => console.log(id),
        classes: {},
        title: "todo",
        type: TasksTypes.toDo,
        id: "task-list-1"
    });

    wrapper.update();
    // should match the snapshot test
    it("should match the snapshot", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    
    //  is to make sure that the component rendered the card UI successfully
    it("should render a material ui paper as a card", () => {
        expect(wrapper.find("[data-test='material-card-test']").exists())
        .toBe(true);
    });

    //  is to make sure that the component rendered the card UI successfully
    it("should render the new task card with input after click on open add mode", () => {
        expect(wrapper.find("[data-test='open-add-mode']")
        .at(1)
        .simulate('click'));

        expect(wrapper.find("[data-test='new-task-card-with-input']").exists())
        .toBe(true);
    });


});
