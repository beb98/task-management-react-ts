import React from "react";
import { mount } from "enzyme";
import { TasksTypes } from "../models/enums/TasksTypes";
import TasksListStore from "../contexts/tasks.store";
import { BoardView } from "../pages/board/Board";

// Create drag and drop mock using jest 
jest.mock('react-beautiful-dnd', () => ({
  Droppable: ({ children } : any) => children({
    draggableProps: {

    },
    innerRef: jest.fn()    
  }, {}),
  Draggable: ({ children } : any) => children({
    draggableProps: {

    },
    innerRef: jest.fn()    
  }, {}),
  // get drag drop context children
  DragDropContext: ({ children } : any) => children,
}));

const initialDataToTestWith = [
    {
        editesHistory: [],
        history: [],
        id: "test-1",
        text: "TEST_TASK_TODO",
        type: TasksTypes.toDo
    },
    {
        editesHistory: [],
        history: [],
        id: "test-2",
        text: "TEST_TASK_IN_PROGRESS",
        type: TasksTypes.inProgress
    },
    {
        editesHistory: [],
        history: [],
        id: "test-3",
        text: "TEST_TASK_DONE",
        type: TasksTypes.done
    }
];


// ###############################################
/* Here we use the same source function that drop tasks, to make the task 
 because as we see {@link https://www.npmjs.com/package/react-beautiful-dnd-test-utils} 
 React dnd and React beautiful dnd doesn't support moving card from a list to another list testing
 the available now is : Moving the dragabble inside the droppable, only positions . 
 original text from npm docs: Currently supports moving a <Draggable /> n positions up or down inside a <Droppable />.   */
// ###############################################
// Start tests for <TaskCard /> component
describe("<Board /> DnD Flow Test", () => {
    const designedAndGetWrapper = (props: any) => mount(
        <TasksListStore>
          <BoardView {...props} />
        </TasksListStore>
      );

    const wrapper = designedAndGetWrapper({
        testInitialData: initialDataToTestWith
    });

    wrapper.update();
    // is to make sure that the component is still the same
    it("should match the snapshot", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    it('toDo tasks can not be drop in done list' , () => {
        // On drop function
        const onDragEnd = (dragEndResult: any) => {
            const { draggableId, source, destination } = dragEndResult;

            switch(source.droppableId) {
                case(TasksTypes.toDo):
                    return dropTodo(draggableId, destination.droppableId);
                case(TasksTypes.inProgress):
                    return true;
                // already tested in Dnd.permissionsToDrag.test.jsx
                case(TasksTypes.done):
                    return false;                    
            }
        }

        const dropTodo = (itemId: string, listType: string) => {
            if (listType === TasksTypes.done) {
                return false;
            }     
            
            return true;                            
        }    

        // start to test based on the test data that passed to the <BoardView /> as initial data
        // try to drag toDo to inProgress
        const dragToDoToInProgress : boolean | undefined = onDragEnd({
            draggableId : initialDataToTestWith[0].id, 
            source : {
                droppableId: initialDataToTestWith[0].type
            }, 
            destination : {
                droppableId: TasksTypes.inProgress
            }
        });

        if (!dragToDoToInProgress) {
            fail('to do tasks can be dropped in in progress list')
        }


        // try to drag inProgress to (toDo, Done)
            // toDo
        const dragInProgressToToDo : boolean | undefined = onDragEnd({
            draggableId : initialDataToTestWith[1].id, 
            source : {
                droppableId: initialDataToTestWith[1].type
            }, 
            destination : {
                droppableId: TasksTypes.toDo
            }
        });

        if (!dragInProgressToToDo) {
            fail('in progress tasks can be dropped in to do list')
        }

            // Done
        const dragInProgressToDone : boolean | undefined = onDragEnd({
            draggableId : initialDataToTestWith[1].id, 
            source : {
                droppableId: initialDataToTestWith[1].type
            }, 
            destination : {
                droppableId: TasksTypes.done
            }
        });

        if (!dragInProgressToDone) {
            fail('in progress tasks can be dropped in to do list')
        }
    
        
        // try to drag toDo to done
        const dragToDoToDone : boolean | undefined = onDragEnd({
            draggableId : initialDataToTestWith[0].id, 
            source : {
                droppableId: initialDataToTestWith[0].type
            }, 
            destination : {
                droppableId: TasksTypes.done
            }
        });
        
        // in drop toDo in Done case we fail if the task done
        if(dragToDoToDone) {
            fail('to do tasks can not be dropped in done list')
        }
        
    })

});
