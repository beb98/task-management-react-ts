import React from 'react';
// @ts-nocheck
import { render } from '@testing-library/react';
import {
  mockGetComputedSpacing,
  mockDndElSpacing,
  makeDnd,
  DND_DIRECTION_RIGHT,
  DND_DIRECTION_LEFT,
  DND_DRAGGABLE_DATA_ATTR
} from 'react-beautiful-dnd-test-utils';
import {BoardView} from '../pages/board/Board';
import { TasksTypes } from '../models/enums/TasksTypes';
import TasksListStore from '../contexts/tasks.store';

const renderApp = () => {
    const BoardComponent = render(<TasksListStore>
                                <BoardView testInitialData={[
                                    {
                                        editesHistory: [],
                                        history: [],
                                        id: "test-1",
                                        text: "TEST_TASK_TODO",
                                        type: TasksTypes.toDo
                                    },
                                    {
                                        editesHistory: [],
                                        history: [],
                                        id: "test-2",
                                        text: "TEST_TASK_IN_PROGRESS",
                                        type: TasksTypes.inProgress
                                    },
                                    {
                                        editesHistory: [],
                                        history: [],
                                        id: "test-3",
                                        text: "TEST_TASK_DONE",
                                        type: TasksTypes.done
                                    }
                                ]} />
                             </TasksListStore>);
  
    mockDndElSpacing(BoardComponent);
  
    const makeGetDragEl = text => () =>
      BoardComponent.getByText(text).closest(DND_DRAGGABLE_DATA_ATTR);
  
    return { makeGetDragEl, ...BoardComponent };
};
  

describe('DnDTest, <BoardView />', () => {

    beforeEach(() => {
        mockGetComputedSpacing();
    });

    it('moves a task inside a tasks list', async () => {
        const { getByText, makeGetDragEl } = renderApp();
  
        await makeDnd({
          getByText,
          getDragEl: makeGetDragEl('TEST_TASK_TODO'),
          direction: DND_DIRECTION_RIGHT
        });

        await makeDnd({
            getByText,
            getDragEl: makeGetDragEl('TEST_TASK_IN_PROGRESS'),
            direction: DND_DIRECTION_RIGHT
        });

        // we use manual fail and try,catch statement to check if the done is draggable or not.
        // we throw error only if the done task is draggable
        try {
            await makeDnd({
                getByText,
                getDragEl: makeGetDragEl('TEST_TASK_DONE'),
                direction: DND_DIRECTION_LEFT
            });
            // Then we fail manually. using custom fail hook function
            fail("FAIL: Done tasks can not be drag");
        }
        catch(e) {            
            return true;
        }        
      });
          
});