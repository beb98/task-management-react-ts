import { makeStyles } from "@material-ui/core";

export const useTaskHistoryDialogStyles = makeStyles((theme) => ({
    listItem : {
        display: "flex",
        justifyContent: "space-around",
        marginBottom : "0.2rem"
    },
    title : {
        width : "80%"
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
        marginLeft : "1rem"
    }
}));