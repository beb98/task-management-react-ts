import React from 'react';
import { Dialog, DialogTitle, Divider, List, ListItem, Typography } from '@material-ui/core';
import { EditesHistory, Task, TaskHistory } from '../../models/Task';
import {useTaskHistoryDialogStyles} from './style';

interface IProps{
    onClose : Function, 
    task : Task, 
    open: boolean
}

// React.FC
const TaskHistoryDialog : React.FC<IProps> = (props) => {    
    
    const classes = useTaskHistoryDialogStyles();
    // IProps
    const { onClose, task, open } = props;

    // Handle close history
    const handleClose = () => {
        onClose(false);
    };    

    return(
        <>
            <Dialog onClose={handleClose} aria-labelledby="task-history" 
                    open={open} fullWidth={true} >
                <DialogTitle id="task-history">
                    <Typography variant="h6" className={classes.title} >
                        {task.text} history and movements
                    </Typography>                
                    <Typography variant="h6" className={classes.title} >
                        Task initial value : {task.editesHistory[0].new}
                    </Typography>                
                </DialogTitle>                
                <List>
                        {
                            task.editesHistory.length > 0 ?
                            task.editesHistory.map((record: EditesHistory, index: number) => (
                                <ListItem key={index} className={classes.listItem}>
                                    {
                                        record.old &&
                                        <>
                                            <span>Edit</span>
                                            <span>Old Value: {record.old}</span>
                                            <span>New Value: {record.new}</span>
                                        </>                                      
                                    }                                    
                                </ListItem>
                            ))
                            :
                            null
                        }

                        <Divider />
                
                        {
                            task.history.length > 0 ?
                            task.history.map((record: TaskHistory, index: number) => (
                                <ListItem key={index} className={classes.listItem}>
                                    <span>From: {record.from}</span>
                                    <span>to: {record.to}</span>
                                    <span>Date: {record.date}</span>
                                </ListItem>
                            ))
                            :
                            null
                        }
                </List>
            </Dialog>
        </>
    )
}

export {TaskHistoryDialog}