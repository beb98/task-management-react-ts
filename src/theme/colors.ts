const colors = {
  transparent: 'transparent',
  boardBackground : "#2c3e50",
  darkBold: '#0D1136',
  primary: '#344AC1',
  secondary: '#0984e3',
  lightGrey: '#dfe6e9',
  darkGrey: "#bdc3c7",
  white: '#fff',
  error : "#d32f2f",
  warning : "#f57c00",
  success : "#388e3c",
  text : "#222"
};

export { colors };