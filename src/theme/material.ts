import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

const theme = createMuiTheme({
  direction :  "ltr",
  palette: {
    primary: {
      main: "#344AC1"
    },
    secondary: {
      main: "#0984e3"
    },
    success: {
      main: "#2ECC71"
    },
    warning: {
      main: "#e67e22"
    },
    error: {
      main: "#E74C3C"
    },
    text: {
      primary: "#222222",
      secondary: "#383838"
    },
  },
  typography: {
    fontFamily: [
      '"Comfortaa"',
      'Roboto',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  overrides: {
    MuiButton : {
      contained : {
        boxShadow : "0px",
        MozBoxShadow : "0px",
        WebkitBoxShadow : "0px",
        borderRadius : "3px"
      }
    }
  }
});


const responsiveTheme = responsiveFontSizes(theme);
export { responsiveTheme}