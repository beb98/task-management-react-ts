import { LocalStorageCommands } from "./localStorageCommands";

//#region Get from localStorage
export const getLocalState = (key: LocalStorageCommands) => {
  try {
    const serializedState = localStorage.getItem(key);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};
//#endregion

//#region Set to localStorage
export const setLocalState = (key : LocalStorageCommands, value : any) => {
  try {
    const serializedState = JSON.stringify(value);
    localStorage.setItem(key, serializedState);
    return true;
  } catch (e) {
    return undefined;
  }
};
//#endregion 