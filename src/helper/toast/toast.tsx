import React from 'react';
import toast from 'toasted-notes' 
import { colors } from '../../theme/colors';
import { MdErrorOutline, MdInfoOutline } from 'react-icons/md';
import { FiX , FiCheckCircle} from 'react-icons/fi';

// Basic alerts
enum BasicTypes {
    error,
    success,
    warning,
    info
}

export const ErrorAlert = (text : string) => {  
    InvokeBasicAlert(text, BasicTypes.error);
}

export const Warninglert = (text : string) => {  
    InvokeBasicAlert(text, BasicTypes.warning);
}

export const SuccessAlert = (text : string) => {  
    InvokeBasicAlert(text, BasicTypes.success);
}

export const InfoAlert = (text : string) => {  
    InvokeBasicAlert(text, BasicTypes.info);
}


const InvokeBasicAlert = (text : string, type : BasicTypes) => {

    let typeColor : string;
    let typeIcon : JSX.Element;
    switch (type) {
        case BasicTypes.error :
            typeColor = colors.error;
            typeIcon = <MdErrorOutline size={20} style={{
                marginRight : "0.3rem"
            }} />
        break;

        case BasicTypes.info : 
            typeColor = colors.primary;
            typeIcon = <MdInfoOutline size={20} style={{
                marginRight : "0.3rem"
            }} />
        break;

        case BasicTypes.warning : 
            typeColor = colors.warning;
            typeIcon = <MdErrorOutline size={20} style={{
                marginRight : "0.3rem"
            }}/>
        break;

        case BasicTypes.success : 
            typeColor = colors.success;
            typeIcon = <FiCheckCircle size={20} style={{
                marginRight : "0.3rem"
            }}/>
        break;
    }

    toast.notify((props) => (
        <div style={{
            background: typeColor,
            color : colors.white, 
            borderRadius : "7px", 
            padding : "0.8rem 1rem", 
            minWidth : "10vw", 
            boxShadow : "0px 7px 14px rgba(0,0,0,0.15)", 
            display : "flex" , 
            justifyContent : "flex-start", 
            alignItems : "center", 
            marginBottom : "1rem", 
            marginRight: "1rem"
        }}>          
          
          {/* alert content */}
          {typeIcon}
          <span style={{padding : "0.3rem", fontWeight : "bold"}}>{text}</span>
                    
          {/*  close button */}
          <FiX size={20} style={{
              marginLeft : "1rem",
              cursor : "pointer"
            }} 
              onClick={props.onClose}/>

        </div>
    ) , {
        duration : 5000,
        position : "bottom-right"
    });
}