import { ThemeProvider } from '@material-ui/core';
import React from 'react';
// Global app stylesheet
import './App.css';
import TasksListStore from './contexts/tasks.store';
// Board container
import { BoardView } from './pages/board/Board';
// Material UI responsive theme style guide
import { responsiveTheme } from './theme/material';


function App() {
  return (
    <div>
      <ThemeProvider theme={responsiveTheme}>
            {/* Global tasks store, Context API store */}
            <TasksListStore>
                {/* Single route app, so we have our content directly */}
                <BoardView />                 
            </TasksListStore>
      </ThemeProvider>
    </div>
  );
}

export default App;
