*NOTE: if you found a message with "no tests found" text after run `yarn test`, 
press 'a' letter from your keyboard. for more details, scroll down to the yarn test part

## some quick technical info 
- we use context API as a global store
- we use yarn as a package manager, feel free to use npm after deleting yarn.lock file
- we use jest and enzyme for unit testing 
- we use typescript for type safety, so that we can impelement more logic
- we use a merged manual and auto runner tests for react-beautiful-dnd 
- react-dnd is hard to test, so we moved to react-beautiful-dnd to use the test-utils libary 
(you can clone any commit. for ex: "DnD added, with all cases")
- look below or in package.json to know how to run the app in dev mode,test or build the app
- a lot of logic saved in the enum that called TaskTypes in ./src/models/enums folder

## our folder structure (inside /src/)
- __tests__ (__snapshots__, componentsFiles.test.tsx/jsx)
- components (seprated UI Components)
- contexts  (for the global state management (global store)
- helper (contain some helper functions like toast,generateGUID,DateFormatting...etc)
- models (contain the types and enums )
- pages (contain the views that have Specific-Route (we have only one >> '/' ))
- theme (contain our theme guidelines in a material ui created theme and in sperated ts file to use it in the material ui stylesheets )


## Project scope 
Task management board with 3 types of lists,
toDo, InProgress and Done. 

- You can move from : 
toDo to InProgress 
InProgress to done
InProgress to toDo

- You can edit the task text in the toDo and InProgress mode only . 
- You can delete any task in the board .
- You can click on calendar to see the movements and edites history .

- when you hover on the card you will see 3 actions (history, edit and delete), edit is not available on Done list.
- there's no ordering feature for the cards in the same list 

## Create react app cli 
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts
In the project directory, you can run:

### `yarn`
### `npm install` or `npm i`
Install all the node_modules .

### `yarn start`
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`
Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

if you found a message with no tests found, 
press 'a'

will run a test for : 
Dnd files
TaskCard,
TaskList, 
NewCardWithInput, 
TaskHistory 

### `yarn build`
Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
